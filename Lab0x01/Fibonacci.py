'''
@file        Fibonacci.py
@brief       Determine the Fibonacci number at any valid index value
@details     This lab is intended to serve as an introduction to Python.
             See code here: https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0x01/Fibonacci.py
@author      Ryan McLaughlin
@date        01/20/21
'''                
            

def fib (idx):
    '''
    @brief          This function calculates a Fibonacci number at a specific index.
    @details        Using a while loop and a bottom up approach solution method,
                    this function determines the Fibonacci number for a given index.
                    Note that this function is only designed to handle valid index values,
                    such that any invalid indecies should be handled by the test program.
    @param          idx A postivie integer specifying the index of the desired Fibonacci number
    @return         Value of Fibonacci number for given index
    '''
    
    n1, n2 = 0, 1           # define the root values for the fibonacci sequence
    
    if idx == 0:            # if the index is 1 or 2, there is no need to use a formula 
        fib = n1            # to calculate the return value
    elif idx == 1:
        fib = n2
    else:
        i = 2               # start at 2 because third value in sequence is sum of first two
        while i <= idx:       # while loop runs until index value is reached
            fib = n1 + n2   
            n1 = n2         # determine next iteration values
            n2 = fib
            i += 1 

    return fib


if __name__ == '__main__':  # only run from "Run file" button

        print('Welcome, please enter a valid Fibonacci index value.')
        
        while True:
            ## @brief       Users must enter a Fibonacci index value, stored as `idx`
            #  @details     Each time the user is prompted for input, the index value input
            #               defines which Fibonacci number will be calculated.
            
            idx = input('Please enter a Fibonacci index value ')
           
            if idx == 'q':  # simple key to exit the program
                break
    
            
            elif idx.isdigit() == False:      
                print('Invalid input, please input a positive integer')
                # restart loop if input is not a positive integer
                
            else:   # if first two condtions do not fail, employ function
                    idx = int(idx)
                    print ('Fibonacci number at '
                       'index {:} is {:}. '
                       'Enter a new index, or press "q" to quit'.format(idx,fib(idx)))
                    
                    