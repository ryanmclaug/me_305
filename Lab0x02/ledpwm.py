'''
@file       ledpwm.py
@brief      Simple program using button press to cycle through three different LED patterns
@details    In our first lab using hardware, the goal was to learn about interupt callbacks, pulse width
            modulation, and some other key basic concepts for working with the Nucleo board. This program
            allows the user to cycle through three different patterns (square, sine, and sawtooth) simply
            by presing the blue button on the Nucleo board. Included below are more details regarding
            source code, the general finite state machine implemented, and user defined functions. 
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0x02/ledpwm.py \n
            \n *See demo video here:* https://youtu.be/7zcb3uxbQ7k \n
            
@image      html Lab0x00STD.jpg " " width = 45%

@author     Ryan McLaughlin
@date       Originally created on 01/19/21 \n Last modified on 01/31/21
'''

import micropython
import pyb
import math
import utime

micropython.alloc_emergency_exception_buf(100)  # display more error messages

# function definitions

def onButtonPressFCN(IRQ_src):  
    '''@brief       Gives button ability to change a variable       
       @details     In order to change the state of the finite state machine, an interrupt callback is used in
                    the form of the blue user button on the Nucleo board. When the callback function is called
                    from the main script, the global variable `button_press` becomes `True`, and this change
                    triggers the main script to update the state.
       @param       IRQ_src is   
    '''     
    print('Button pressed')   
    global button_press
    button_press = True
    
    
def square(tdiff):
    '''@brief       Square wave pattern function  
       @details     The first of three pulsing waveforms is the classic square wave. For this wave, the formula, \n \n
                    <CENTER>duty = 100 * ( (t%1) <= 0.5 )</CENTER> \n \n is employed. The modulus operator reduces the
                    input to a value between 0 and 1 (discluding 1), followed by a comparision with 0.5. With this,
                    the LED will switch between on and off every half second.
       @param       tdiff See the above function `sine()` for a full description of `tdiff`
    '''  

    duty = 100*( (tdiff%1)<=.5)
    t2ch1.pulse_width_percent(duty)


def sine(tdiff):
    '''@brief       Sine wave pattern function  
       @details     The second waveform shown on the LED is a shifted sine wave. For this wave, the formula, \n \n
                    <CENTER>duty = 50 * ( 1 + sin( .2π*t ) )</CENTER> \n \n is employed. Since a sine wave is
                    inherently continuous in nature, operators are not needed in this function to modify the input
                    time value. This wave has an amplitude of 50 (to oscillate between 0% and 100%) and is shifted
                    upward to begin at 50% brightness.
       @param       tdiff See the above function `sine()` for a full description of `tdiff`   
    '''  

    duty = 50 * ( 1 + math.sin( .2*math.pi*tdiff ))    # .2 from radian freq of (2π rad/10 sec)
    t2ch1.pulse_width_percent(duty)


def sawtooth(tdiff):
    '''@brief       Sawtooth wave pattern function  
       @details     The final waveform in the three pattern sequence is a sawtooth (or ramping) waveform. For this wave, 
                    the formula, \n \n <CENTER>duty = 100 * ( t%1 )</CENTER> \n \n is employed. The modulus operator
                    reduces the input time to a vavlue between 0 and 1 (discluding 1), which can then be multiplied by
                    100 to get a correct brightness value.
       @param       tdiff Calculated in the main script, this is the time elapsed since the button was last pressed.
                    The same input parameter is used for each of the three functions, and is calculated using a 
                    combination of `utime.ticks_ms()` and `utime.ticks_diff()`.      
    '''

    duty = 100*(tdiff%1)
    t2ch1.pulse_width_percent(duty)

      


# main program/test program begin

if __name__ == "__main__":
    
    ## @brief       Boolean variable to define if the button is pressed
    #  @details     When the button has been pressed, the callback function is
    #               implemented to change `button_press` to `True`. This variable
    #               therefore defines when to switch states.

    button_press = False        # initially button is not pressed
    
    ## @brief       Boolean variable to define if the button is pressed
    #  @details     When the button has been pressed, the callback function is
    #               implemented to change `button_press` to `True`. This variable
    #               therefore defines when to switch states.
    state = 0                   # set initial state to zero
    
    ## @brief       Starts timer zero point  
    #  @details     To later calculate time elapsed since a state has been entered, a timer needs to start running.
   
    t0 = utime.ticks_ms()
    
    print('Welcome to the LED pattern program. Press the blue button (B1) on the Nucleo board '
          'to cycle through the square wave, sine wave, or sawtooth pattern. To exit the program, '
          'press "Ctrl-C" at any time.')
    
    ## @brief      Pin variable for blue user button 
    #  @details    The blue button on the board serves as the main user input interface,
    #              and therefore needs a pin assignment.
                 
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)                      # create pin object for PC13, blue button
    
    ## @brief      Pin variable for LED 
    #  @details    Similar to `pinC13`, but for the output LED. 
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)                        # create pin object for PA5, LED
    
    ## @brief       Pin variable for pyb timer     
    #  @details     To use pulse width modulation of the LED, we need a timer.

    tim2 = pyb.Timer(2, freq = 20000)                       # define pin timer
    
    ## @brief       Channel variable for pyb timer

    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)       # create timer channel
    
    ## @brief       Callback interrupt variable
    #  @details     When the blue button is pressed, it triggers the callback variable,
    #               running the callback function `onButtonPressFCN`.

    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

    while True:
        try:

            
            # check if button has been pressed
            if button_press == True:
                
                
                t0 = utime.ticks_ms()           # set t0 point
                
                if (state == 0 or state == 3):
                    state = 1
                    print('Square wave pattern selected')
                    
                elif state == 1:
                    state = 2
                    print('Sine wave pattern selected')
                    
                else:
                    state = 3
                    print('Sawtooth pattern selected')
                    
                button_press = False           # reset button_press
                    
            # end first loop, state has been set
            

            
            ## @brief     Time elapsed since last button press    
            #  @details   Since each pattern function is based on time, td is passed through
            #             to the correct function, and is used to calculate what brightness
            #             level the LED should be set to.
                  
            td = 0.001*utime.ticks_diff(utime.ticks_ms(),t0)    # calculate time since last button press
            
            
            # check state, run proper function
            if state == 0:
                t2ch1.pulse_width_percent(0)                    # led off in init state

            elif state == 1:
                
                square(td)

            elif state==2:
                
                sine(td)
 
            elif state==3:
                
                sawtooth(td)
                    
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
        except KeyboardInterrupt:       # press Ctrl-C to interupt 
            print('Ctrl-C has been pressed, exiting program')
            t2ch1.pulse_width_percent(0)          
            break