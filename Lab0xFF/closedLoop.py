'''
@file       closedLoop.py
@brief      Controller driver class
@details    This file contains a controller class based on a PI contoroller
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/closedLoop.py \n

@author     Ryan McLaughlin
@date       Originally created on 03/10/21 \n Last modified on 03/17/21
'''



class closedLoop:
    ''' 
    @brief This class implements a controller driver for theME305 board.
    '''

    def __init__ (self, KpPrime, KiPrime, debugFlag = False):
        '''
        @brief  Constructor for controller driver class
        @param  KpPrime Velocity feedback constant
        @param  KiPrime Position feedback constant
        @param debugFlag Debugging flag related to print statements in Putty
        '''
        
        print ('Creating a controller driver')
        
        ## @brief  Debugging flag for print statements in Putty
        self.debugFlag = debugFlag
        
        ## @brief Velocity feedback constant
        self.KpPrime = KpPrime
        
        ## @brief Position feedback constant
        self.KiPrime = KiPrime
             
        
    def update(self, omegaRef, omega, thetaRef, theta):
        '''
        @brief  Update method for controller
        @details  This method is run on every period of the controller task, and takes in desired and current
                  speed and position to get a new PWM level for the motors.
        @param    omegaRef  Reference speed, interpolated at current time from reference data set
        @param    omega     Current speed, called from encoder driver object
        @param    thetaRef  Reference position, interpolated at current time from reference data set
        @param    theta     Current position, called from encoder driver object
        @return   Desired PWM level for motor
        '''

        L = self.KpPrime*(omegaRef - omega) + self.KiPrime*(thetaRef - theta)
        
        print('closedLoop: L = ' + str(L))
        
        return L
    
    
    def get_Kp(self):
        '''
        @brief Method for calling current Kp value
        @return  Current gain value
        '''
        return self.KpPrime
    
    
    def set_Kp(self,newKpPrime):
        '''
        @brief      Method for setting Kp
        @param      newKpPrime   New gain value
        '''
        self.KpPrime = newKpPrime
        
            
