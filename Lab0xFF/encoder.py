'''
@file       encoder.py
@brief      Encoder driver class
@details    Class for implementing methods related to encoders, such as getting the most recent position or
            speed or updating the count.
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/encoder.py \n

@author     Ryan McLaughlin
@date       Originally created on 02/21/21 \n Last modified on 03/17/21
'''

import pyb
import utime

class encoder():
    '''
    @brief      Encoder driver class
    @details    Class for implementing methods related to encoders, such as getting the most recent position or
                speed or updating the count.
    '''
    
    
    def __init__(self, pin1, pin2, timerNum, debugFlag = False):
        '''
        @brief      Constructor for class `encoder`
        @param      pin1 Encoder pin # 1
        @param      pin2 Encoder pin # 2
        @param      timerNum Corresponding timer based on datasheet
        @param      debugFlag   Debugging flag set to `True` or `False`
        '''
        
        ## @brief  Debugging flag for print statements to be shown in Putty
        self.debugFlag = debugFlag
        
        ## @brief  Timer number on Nucleo board for encoder
        self.timerNum = timerNum
        
        ## @brief  Pin 1 for encoder
        self.pin1 = pyb.Pin (pin1, pyb.Pin.IN)       # define pin mode
        
        ## @brief  Pin 2 for encoder
        self.pin2 = pyb.Pin (pin2, pyb.Pin.IN)       
        
        ## @brief       definition of timer
        #  @details     Period of 35535 (encoder count before overflow occurs), prescaler of 0
        self.tim = pyb.Timer(timerNum, prescaler = 0, period = 0xffff, 
                             mode = pyb.Timer.UP, div = 1, deadtime = 0)        # timer definition
        
        ## @brief   First encoder timer channel
        self.tim_ch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin = self.pin1)        # channel definitions
        
        ## @brief   Second encoder timer channel
        self.tim_ch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin = self.pin2)
        
        ## @brief  Previous time, used for determining speed
        self.lastTime = utime.ticks_us()
        
        ## @brief  Current absolute time in microseconds
        self.currentTime = utime.ticks_us()
        
        ## @brief  Last position, updated at the end of each encoder update
        self.lastPos = self.tim.counter()           # set last position, first iteration will use this value (should be zero)
        
        ## @brief  Current angle (in degrees) of encoder
        self.theta = 0
        
        ## @brief  Current speed (in rpm) of encoder
        self.speed = 0
        
        
        
    def update(self):
        '''
        @brief      Updates encoder position
        @details    This method implements an alogrithm developed in lecture for interpreting tick count changes into
                    correct changes in encoder angular displacement. The encoder count can overflow or underflow, so
                    these values are not enough on their own.
        '''
        
        
        
        self.currentTime = utime.ticks_us()
        
        ## @brief  Time between updates, used for determining speed
        self.realPeriod = utime.ticks_diff(self.currentTime,self.lastTime)
        
        if self.debugFlag == True:
            print('encoder: realPeriod is ' + str(self.realPeriod))
        
        # 1) measure encoder count
        ## @brief Current encoder count
        self.currentPos = self.tim.counter()        # in other words get new encoder count, unmodified
        print('encoder: timer counter for pyb.Timer({:}) = {:}'.format(self.timerNum,self.currentPos))
        
        # 2) compare to previous count, find delta value
        
        
        ## @brief    Difference in encoder count since last update
        #  @details  This value will be adjusted based on algorithm
        self.deltaTicks = self.currentPos - self.lastPos
        print('encoder: unmodified delta value is ' + str(self.deltaTicks))
        
        # 3) validate delta
        if  self.deltaTicks > 0.5*self.tim.period():
            self.deltaTicks -= self.tim.period()
            print('encoder: case 2, underflow')
                
        elif self.deltaTicks < -0.5*self.tim.period():
            self.deltaTicks += self.tim.period()
            print('encoder: case 3, overflow')
        
        else:
            print('encoder: case 1, no correction needed')
        
        
        print('encoder: modified delta value is ' + str(self.deltaTicks) + ' ticks.')
        # 3.5) speed
        # print('encoder: delta is ' + str(self.deltaTicks)) 
        if self.realPeriod > 0:  
            self.speed = float(self.deltaTicks)/float(self.realPeriod/1000000) * 60/4000

        # 4) after fixing delta, accumulate (add up) in degrees
        self.theta += self.deltaTicks*36/400                 # theta_new = theta_old + delta
        
        if self.debugFlag == True:
            print('encoder: theta value is ' + str(self.theta))
            
        # 5) store currentPos as last position, since next iteration will get a new current position value
        self.lastPos = self.currentPos          # update last position for next iteration
        self.lastTime = self.currentTime
        
        
        
        
    def get_position(self):
        '''
        @brief      Retrieves the current encoder position
        @details    Used by the controller task, as well as being callable by key press from the PC
                    script
        @return     Theta, current encoder position
        '''
        return self.theta
        
    def set_position(self,newPosition):
        '''
        @brief      Sets the encoder position
        @details    Can be called from the front end to zero an encoder position
        @param      newPosition     Takes in the new desired encoder position value  
        '''
        self.theta = newPosition
    
    def get_delta(self):
        '''
        @brief      Retrieves the last change in encoder position (in ticks, not angle)
        @details    Can be called from the PC side
                    script
        @return     Delta, last adjusted change in enooder count
        '''
        return self.deltaTicks
    
    def get_speed(self):
        '''
        @brief      Gets the current encoder speed in rpm
        @details    Used by the controller task, as well as being callable by key press from the PC
                    script
        @return     Omega, current speed measured by encoder
        '''
        return self.speed
        