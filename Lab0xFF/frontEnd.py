'''
@file       frontEnd.py
@brief      PC Script for Lab 0x0FF
@details    This file serves to run the PC (or front end) side of operations for our lab. From this script,
            keystroke commands are sent to the Nucleo, data is processed into a .csv file, and data is plotted
            to produce a .png file
            
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/frontEnd.py \n
            \n *Example output .csv file here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/outputDataExample.csv \n

@image      html BestPlot.png " " width = 40%
@details    Shown above is an example output plot from `frontEnd.py`

@author     Ryan McLaughlin
@date       Originally created on 02/18/21 \n Last modified on 03/17/21
'''

import serial
import csv
import keyboard
import os
import matplotlib.pyplot as plt
import numpy as np


# keyboard callback function
def kb_cb(key):
    '''
    @brief      Callback method based on keyboard inputs
    @details    In order to use the keyboard to set certain tasks into motion, a callback is used, along with
                the keyboard module for python. 
    @param      key is the key released by the user
    '''
    global lastKey
    lastKey = key.name


def sendChar(lastKey):
    '''
    @brief      Method for sending characters via serial to Nucleo
    @details    `sendChar` writes characters from the PC to the Nucleo by encoding as ascii characters,
                then decoding
    @param      lastKey is the last key released on the keyboard.
    '''
    ser.write(lastKey.encode())


def clearFiles(fileNames):
    '''
    @brief      Method for deleting files
    @details    Since the csv writer is set to append to a certain file, the file to be written to must be removed
                each time the PC collects new data. If it was desired, this could be replaced by a method to rename
                the next file to write to a new file each time. For this, the `os` module is used.
    @param      fileNames is a list of strings containing the various files written each time the frontEnd is run 
    '''    
    _filesRemoved = 0
    
    for n in range(len(fileNames)):
        
        _fileName = fileNames[n]
        
    # check if file exists 
        if os.path.exists(_fileName):
            os.remove(_fileName)
            _filesRemoved += 1
            
    print('{:} .csv/.png files have been removed'.format(_filesRemoved))       
     
    
# recieve from uart, write to csv file
def writeCSV(fromNucleo,csvFileName):
    '''
    @brief      Method for writing a .csv file
    @details    As data is being transferred from the Nucleo to the PC, we want to append this data
                to a .csv file for later use/analyis. Initially, the string is already in a csv type format,
                but to write it to a csv file, we need to strip the string of line endings, split it on commas,
                convert to type `float`, then append a new row to the .csv file.
    @param      fromNucleo is the string read from uart for a given iteration
    @param      csvFileName is the specified file name of the output .csv file
    '''  
    csvStripped = fromNucleo.strip()                 # strip line endings
    csvSplit = csvStripped.split(',')               # split on commas
    
    time            = float(csvSplit[0])                  # float time and function values to be written to csv
    enc1pos         = float(csvSplit[1])
    enc1speed       = float(csvSplit[2])
    enc2pos         = float(csvSplit[3])
    enc2speed       = float(csvSplit[4])
    

    with open(csvFileName,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([time,enc1pos,enc1speed,enc2pos,enc2speed]) 
    
    
def createPlot(csvFileName,pngFileName,figureCaption,posCol,speedCol,tRef,speedRef,posRef,Kp,Ki,J):
    '''
    @brief      Method for creating a plot
    @details    This method utilizes the matplotlib module to plot data from a .csv file created
                earlier. The .csv file is used for plotting in conjunction with the numpy module,
                and then the plot is saved to a .png file in the working directory.
    @param      csvFileName is the specified file name of the output .csv file
    @param      pngFileName is the specified file name of the output .png file
    @param      figureCaption The desired caption for the plot
    @param      posCol  Column in output csv file containing speed data for the appropriate encoder
    @param      speedCol  Column in output csv file containing position data for the appropriate encoder
    @param      tRef  Refrence time data
    @param      speedRef  Reference speed data
    @param      posRef    Reference position data
    @param      Kp      Gain value for speed feedback
    @param      Ki      Gain value for position feedback
    @param      J       Performance metric
    '''  
    
    # formatting
    _myfont = {'fontname':'Times New Roman','fontsize':12}
    
    print('Plotting data')
    figure1 = plt.figure(figsize=(7, 6), dpi=200)   # create a figure
    plt.subplot(211)                                # subplot 1                     
    x1 = []
    y1 = []
    x1, y1 = np.loadtxt(csvFileName, delimiter=',',usecols=(0, speedCol), unpack=True)
    plt.plot(x1,y1,'k')
    plt.plot(tRef, speedRef,'b')
    

    _speedMin = 1.1*float(min([min(y1), min(speedRef)]))
    _speedMax = 1.1*float(max([max(y1), max(speedRef)]))

    
    plt.axis([0, max(tRef), _speedMin, _speedMax ])
    
    plt.xlabel('time, t [ s ]\n\n',**_myfont)
    plt.ylabel('output shaft speed, Ω [ rpm ]',**_myfont)
    
    plt.subplot(212)                                # subplot 1                     
    x2 = []
    y2 = []
    x2, y2 = np.loadtxt(csvFileName, delimiter=',',usecols=(0, posCol), unpack=True)
    plt.plot(x2,y2,'k')
    
    plt.plot(tRef, posRef,'b')
    
    _posMin = 1.1*float(min([min(y2), min(posRef)]))
    _posMax = 1.1*float(max([max(y2), max(posRef)]))
        
    plt.axis([0, max(tRef), _posMin, _posMax])
    
    plt.text(4,2000,'Kp = {:}, Ki = {:}, J = {:}'.format(Kp,Ki,J),**_myfont)
    
    plt.xlabel('time, t [ s ]\n\n' + figureCaption,**_myfont)
    plt.ylabel('output shaft angle, Θ [ deg ]',**_myfont)
    
    plt.tight_layout()
    plt.show()

    figure1.savefig(pngFileName)
    print('Plotting completed')


def sampleCSV(referenceCSV, modified, controllerPeriod):
    '''
    @brief      Method for resampling reference data at lower resolution
    @details    This method uses the numpy library to resample a refrence data set for reference tracking.
                Instead of loading in every line of a given csv file, an increasing number of rows are
                skipped over, and a single line is read each time through the loop, creating a lower resolution
                data set (so that memory issues do not occur on the Nucleo).
    @param      referenceCSV     Original file name
    @param      modified        Modified file name
    @param      controllerPeriod  Needed to determine at what rate to resample
    @return     Lists for time, speed, and position that can be used for plotting
    '''
    
    skip = controllerPeriod/1000
    points = int(15/skip*1000) + 1
    print('Sampling {:} data points, at a sample period of {:} seconds'.format(points,float(skip/1000)))
    
    tRef = []
    omegaRef = []
    thetaRef = []
    
    with open(modified,"a") as f:
        for n in range(points):
            
            n0 = int(n*skip)
            tRef0, omegaRef0, thetaRef0 = np.loadtxt(referenceCSV, delimiter=',', skiprows = n0,  unpack=True, max_rows=1)
            tRef.append(tRef0)
            omegaRef.append(omegaRef0)
            thetaRef.append(thetaRef0)
            
            writer = csv.writer(f,delimiter=",")
            writer.writerow([tRef0,omegaRef0,thetaRef0])

    return tRef, omegaRef, thetaRef


if __name__ == "__main__":
    
    _csvName = "encData.csv"
    _csvReference = "modifiedRef.csv"
    _pngName1 = "enc1Plot.png"
    _pngName2 = "enc2Plot.png"
    
    _figureCaption1 = 'Figure 1. Encoder 1 position and velocity as a function of time'
    _figureCaption2 = 'Figure 2. Encoder 2 position and velocity as a function of time'
    
    _fileNames = [_csvName,_csvReference,_pngName1,_pngName2]
    
    clearFiles(_fileNames)
    
    ## @brief  Reference time list
    t = []
    
    ## @brief  Reference speed list
    omega = []
    
    ## @brief  Reference position list
    theta = []
    
    t, omega, theta = sampleCSV("reference.csv", _csvReference, 25000)
    
    
    
    ## @brief     Serial port definition
    #  @details   Need to specify the port to use for serial communication, as well as a baud rate
    ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
    ser.flushInput()

    
    # Tell the keyboard module to respond to these particular keys only
    ## @brief   Define callback keys
    #  @details Using `keyboard.on_release_key` with "s" and "g" to implement callbacks from key presses
    callbackKeys = ["s","g","z","p","d","v","x","o","f","w","r"] 
    
    ##  @brief  callback for keys
    for k in range(len(callbackKeys)):
        keyboard.on_release_key(callbackKeys[k], callback = kb_cb)
    
    
    ## @brief   Initial value of last key released
    #  @details Setup such that key press callback is only checked if `lastKey` is not None.
    lastKey = None
    
    ## @brief  Boolean variable for displaying UI options
    #  @details   Intitially set to `True`, the user prompt message is displayed following
    #             any key press to inform the user of possible options
    userPrompt = True
    
    ## @brief  Debugging flag for print statements in Spyder
    debugFlag = False
    
    ## @brief  Kp value sent from UI
    Kp = 0

    ## @brief  Ki value sent from UI    
    Ki = 0
    
    ## @brief  J value for encoder 1 sent from UI
    J1 = 0
    
    ## @brief  J value for encoder 2 sent from UI
    J2 = 0
    
    while True:
        
        try:
            
            if userPrompt == True:
                
                print('Available commands:\n'
                      'g -> Begin data collection   s -> Stop data collection\n'
                      'z -> Zero encoder 1          x -> Zero encoder 2\n'
                      'p -> Get encoder 1 position  o -> Get encoder 2 position\n'
                      'd -> Get encoder 1 delta     f -> Get encoder 2 delta\n'
                      'v -> Get encoder 1 speed     w -> Get encoder 2 speed\n'
                      'r -> Disable both motors\n')
                
                
                userPrompt = False
            
            # start by checking for key press
            
            ## @brief  Most recent line read from serial communciation between the PC and the Nucleo
            fromNucleo = ser.readline().decode('ascii')    # read from uart

            
            if fromNucleo:
                if debugFlag == True:
                    print('fromNucleo is not empty')
                
                ## @brief Non empty line from Nucleo serial communication
                currentFromNucleo = fromNucleo
                
                if currentFromNucleo == 'plot\r\n':
                    createPlot(_csvName,_pngName1,_figureCaption1,1,2,t,omega,theta,Kp,Ki,J1)      # input integers represent columns of csv file
                    createPlot(_csvName,_pngName2,_figureCaption2,3,4,t,omega,theta,Kp,Ki,J2)
                    print('All actions completed, closing program')
                    break
                
                elif currentFromNucleo[0] == 'E':      # if first letter of message from myuart is a captial E
                    # using this as a catch for "Encoder...", otherwise what we want printed will instead write to csv
                    print(currentFromNucleo)   
                
                elif currentFromNucleo[0] == 'J':   # sending Kp, Ki, J's with this
                    
                    ## @brief CSV of Kp, Ki, and J values
                    metrics = currentFromNucleo[1:]
                    metrics = metrics.strip()                 # strip line endings
                    metrics = metrics.split(',') 
                    Kp = float(metrics[0])
                    Ki = float(metrics[1])
                    J1 = float(metrics[2])
                    J2 = float(metrics[3])
                    
                else:   # this case is for writing to csv file
                    if debugFlag == True:
                        print('writing ' + currentFromNucleo + ' to csv file')
            
                    writeCSV(currentFromNucleo,_csvName)
                    

     
            if lastKey is not None:    # if a key is pressed, transition to new state
                
                sendChar(lastKey)    
                print('You pressed ' + lastKey)
                lastKey = None     # reset lastKey
                userPrompt = True
                
                
        except KeyboardInterrupt:
            print('Ctrl-C has been pressed, exiting program prematurely')
            break
        
    
    ser.close()
    
