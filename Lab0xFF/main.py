'''
@file       main.py
@brief      Nucleo main script for Lab0xFF, Week 1
@details    This file runs an instance of class `nucleoClass`
            
@author     Ryan McLaughlin
@date       Originally created on 02/18/21 \n Last modified on 03/17/21
'''

import shares
from uiTask import uiTask
from controllerTask import controllerTask

if __name__ == "__main__":
    
    controlPeriod = 20000
    uiPeriod0     = 11000
    
    task1 = controllerTask( controllerPeriod = controlPeriod, debugFlag = True)
    task2 = uiTask( timeSpan = 15000000 , uiPeriod = uiPeriod0 , encoderPeriod = controlPeriod, debugFlag = False )
    

    while True: 
        try: 
        
            task1.run()
            task2.run() 

            
        except KeyboardInterrupt:
            # break on ctrl-C
            break