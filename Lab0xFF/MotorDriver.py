'''
@file       motorDriver.py
@brief      Motor driver class
@details    Implements mehtods related to motor operation such as setting and getting
            PWM level
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/MotorDriver.py \n

@author     Ryan McLaughlin
@date       Originally created on 03/02/21 \n Last modified on 03/17/21
'''

import pyb


class motorDriver:
    ''' 
    @brief This class implements a motor driver for theME305 board.
    '''

    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timerChA, timerChB, timerNum, motorNum, debugFlag = False):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        @param timerChA  First timer channel number
        @param timerChA  Second timer channel number
        @param timerNum  Corresponding timer number for motor
        @param motorNum  Used in print statements to identify motors from each other
        @param debugFlag Debugging flag related to print statements in Putty
        '''
        
        
        print ('Creating a motor driver')
        
        ## @brief   1 or 2, used for motor identification in Putty window
        self.motorNum = motorNum
        
        ## @brief  Debugging flag for print statements in Putty
        self.debugFlag = debugFlag
        
        ## @brief  The enable/disable pin for the motor
        self.nSLEEP_pin = pyb.Pin ( nSLEEP_pin, pyb.Pin.OUT_PP )    # nSleep
        
        ## @brief  Input pin # 1
        self.IN1_pin = pyb.Pin ( IN1_pin )      # IN1
        
        ## @brief   Input pin # 2
        self.IN2_pin = pyb.Pin ( IN2_pin )      # IN2
        
        ## @brief   A pyb.Timer object
        self.tim = pyb.Timer( timerNum, freq = 20000 )
        
        ## @brief   First timer channel
        self.t3chA = self.tim.channel(timerChA, pyb.Timer.PWM, pin = self.IN1_pin)
        
        ## @brief   Second timer channel
        self.t3chB = self.tim.channel(timerChB, pyb.Timer.PWM, pin = self.IN2_pin)
        
        ## @brief   Duty cycle (-100 to 100 allowed) of motor
        self.duty = 0

                
        
    
    def enable (self):
        '''@brief  Sets nSleep pin high, enabling the motors
        '''
        if self.debugFlag == True:
            print ('motorDriver: Enabling Motor')
        self.nSLEEP_pin.high()
        
        
    def disable (self):
        '''@brief  Sets nSleep pin low, disabling the motors
        '''
        if self.debugFlag == True:
            print ('motorDriver: Disabling Motor')
        self.nSLEEP_pin.low()
        
        
    def setDuty (self, duty):
        ''' 
        @brief    Method for setting motor duty cycle/PWM level
        @details  This method sets the duty cycle to be sent to the motor to the given level. Positive values
                  cause effort in one direction, negative values in the opposite direction.
        @param    duty    A float holding the duty cycle of the PWM signal sent to the motor
        '''
        
        if self.debugFlag == True:
            print('MotorDriver: attempting to set motor duty cycle to ' + str(duty) + '%')
        
        if duty > 100:
             A = 100
             B = 0  
             
        elif duty < -100:
             A = 0
             B = 100
            
        elif 0 <= duty <= 100:
            A = duty
            B = 0
        
        elif -100 < duty < 0:
            A = 0
            B = -duty            
   
        self.t3chA.pulse_width_percent(A)
        self.t3chB.pulse_width_percent(B)

        self.duty = duty
    
    def getDuty(self):
        '''
        @brief  Method for retrieving the current duty cycle
        @return  Signed float between -100 and 100 representing the PWM level
        '''    
        return self.duty
    
    
