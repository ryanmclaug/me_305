# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 17:44:02 2021

@author: rmcla
"""

import keyboard
import time


last_key = None

def kb_cb(key):
    """ Callback function which is called when a key has been pressed.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("s", callback=kb_cb)
keyboard.on_release_key("g", callback=kb_cb)

time.sleep(1)
keyboard.release('g')


# Run this loop forever, or at least until someone presses control-C
while True:
    try:
        if last_key is not None:
            print("You pressed " + last_key)
            last_key = None

    except KeyboardInterrupt:
        break

# Turn off the callbacks so next time we run things behave as expected
keyboard.unhook_all()
