# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 09:10:14 2021

@author: Charlie
"""

from matplotlib import pyplot
from array import array
from math import exp, sin, pi

# Generate a few arrays to hold the data to plot. Here the arrays
# are set to use floats as the data type by specifying 'f' as the
# first argument. 
times  = array('f', list(time/100 for time in range(0,3001)))

# We need a place to store the data values as well.
values = array('f', 3001*[0])

n=0

# We could do this with an iterator, but a for loop is OK too.
for time in times:
    values[n] = exp(-time/10)*sin(2*pi/3*time)
    n += 1

pyplot.figure()
pyplot.plot(times, values)
pyplot.xlabel('Time')
pyplot.ylabel('Data')

# As an example, print the data as a CSV to the console
for n in range(len(times)):
    print('{:}, {:}'.format(times[n], values[n]))
    # print(str(times[n]) + ', ' + str(values[n]))

# As another example, build a single line string, strip special characters,
# split on the commas, then float the entries
num1 = 1
num2 = 2
# Generate string (you will read this with ser.read() in your PC script)
myLineString = '{:}, {:}\r\n'.format(num1, num2)

# Remove line endings
myStrippedString = myLineString.strip()

# split on the commas
mySplitStrings = myStrippedString.split(',')

# Convert entries to numbers from strings
myNum1 = float(mySplitStrings[0])
print(myNum1)
myNum2 = float(mySplitStrings[1])
print(myNum2)