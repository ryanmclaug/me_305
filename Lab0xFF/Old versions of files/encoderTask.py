'''
@file       encoderTask.py
@brief      
@details    
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/encoderTask.py \n

@author     Ryan McLaughlin
@date       Originally created on 03/02/21 \n Last modified on 03/02/21
'''

import shares
import utime
import pyb
from encoder import encoder

# define encoders
enc1 = encoder( pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, debugFlag = False)
enc2 = encoder( pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, debugFlag = False) 


class encoderTask:
    
    # static variables here
    S0_INIT             = 0
    S1_READ_DATA        = 1
    
    
    def __init__(self, period = 25000, debugFlag = False):
        
        self.debugFlag = debugFlag
        
        ## @brief       Finite state machine needs a state variable to regularly update as state transitions occur.
        self.state = 0   
        
        self.period = period
        
        self.lastTime = utime.ticks_us()
        
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        self.runs = 0
        
        pass
    
    def run(self):
          
        
        self.currentTime = utime.ticks_us()
        
        if self.state == self.S0_INIT:
                
            self.transitionTo(self.S1_READ_DATA)
        
        
        elif self.state == self.S1_READ_DATA:
            
            # if task period is reached
            if utime.ticks_diff(self.currentTime, self.nextTime) >= 0:
                print('encoderTask: period reached')
                
                enc1.update()
                enc2.update()  
                
                # Timestamp for the next iteration of the task
                
                self.nextTime = utime.ticks_add(self.nextTime, self.period)
                self.runs += 1
                
                # encoderTask period is reached, need to get new data point 
                print('encoderTask: determining new enc1data value')
                # shares.enc1Data = []
                enc1Pos = enc1.get_position()
                print('encoderTask: enc1 position is ' + str(enc1Pos))
                shares.enc1Data.append(enc1Pos)    # place value in list, to be read by uiTask
                
                
                # shares.enc2Data = []                          
                print('encoderTask: determining new enc2data value')
                enc2Pos = enc2.get_position()
                shares.enc2Data.append(enc2Pos) 
                
            # print('shares.command is ' + str(shares.command) )       
                
            
            # if shares.command is something besides None, want to obey that command
            if len(shares.command) != 0:
                print('encoderTask: keyboard command sent from shares')
                currentCommand = shares.command.pop(0)
                
                # commands for encoder 1
                if currentCommand == "zeroEnc1":
                    print('encoderTask: zero encoder 1')
                    enc1.set_position(0)
                    shares.forPrint.append("Encoder 1 postiion reset to zero\r\n")
                    
                elif currentCommand == "posEnc1":
                    print('encoderTask: getting position for encoder 1')
                    shares.forPrint.append("Encoder 1 position is " + str(enc1.get_position()) + " ticks\r\n")
                
                elif currentCommand == "deltaEnc1":
                    print('encoderTask: getting delta for encoder 1')
                    shares.forPrint.append("Encoder 1 delta is " + str(enc1.get_delta()) + " ticks\r\n")
                
                elif currentCommand == "speedEnc1":
                    print('encoderTask: getting speed for encoder 1')
                    shares.forPrint.append("Encoder 1 speed is " + str(enc1.get_speed()) + " rad/s\r\n")
                
                
                # commands for encoder 2
                elif currentCommand == "zeroEnc2":
                    print('encoderTask: zero encoder 2')
                    enc2.set_position(0)
                    shares.forPrint.append("Encoder 2 postiion reset to zero")
                    
                elif currentCommand == "posEnc2":
                    print('encoderTask: getting position for encoder 2')
                    shares.forPrint.append("Encoder 2 position is " + str(enc2.get_position()) + " ticks\r\n")
                
                elif currentCommand == "deltaEnc2":
                    print('encoderTask: getting delta for encoder 2')
                    shares.forPrint.append("Encoder 2 delta is " + str(enc2.get_delta()) + " ticks\r\n")
                
                elif currentCommand == "speedEnc2":
                    print('encoderTask: getting speed for encoder 2')
                    shares.forPrint.append("Encoder 2 speed is " + str(enc2.get_speed()) + " rad/s\r\n")
                
                print('encoderTask: added the following string to shares.forPrint -> ' + str(shares.forPrint[-1]) )
                
                
                
    
    
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState is the state to transition to
        '''
        if self.debugFlag == True:
            print('encoderTask: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            print('encoderTask: State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state
        