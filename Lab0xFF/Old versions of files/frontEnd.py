'''
@file       frontEnd.py
@brief      PC Script for Lab 0x0FF, week 1
@details    This file serves to run the PC (or front end) side of operations for our lab. From this script,
            keystroke commands are sent to the Nucleo, data is processed into a .csv file, and data is plotted
            to produce a .png file
            
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/frontEnd.py \n
            \n *Example output .csv file here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/functionDataExample.csv \n

@image      html decayFunction.png " " width = 50%
@details    Shown above in Figure 2 is an example output plot from `frontEnd.py`

@author     Ryan McLaughlin
@date       Originally created on 02/18/21 \n Last modified on 02/24/21
'''

import serial
import csv
import keyboard
import os
import matplotlib.pyplot as plt
import numpy as np

# keyboard callback function
def kb_cb(key):
    '''
    @brief      Callback method based on keyboard inputs
    @details    In order to use the keyboard to set certain tasks into motion, a callback is used, along with
                the keyboard module for python. Only an "s" or "g" will be able to trigger the callback.
    @param      key is the key released by the user
    '''
    global lastKey
    lastKey = key.name

def sendChar(lastKey):
    '''
    @brief      Method for sending characters via serial to Nucleo
    @details    `sendChar` writes characters from the PC to the Nucleo by encoding as ascii characters,
                then decoding
    @param      lastKey is the last key released on the keyboard.
    '''
    ser.write(lastKey.encode())
    myval = ser.readline().decode('ascii')
    return myval

def clearFiles(csvFileName,pngFileName):
    '''
    @brief      Method for deleting files
    @details    Since the csv writer is set to append to a certain file, the file to be written to must be removed
                each time the PC collects new data. If it was desired, this could be replaced by a method to rename
                the next file to write to a new file each time. For this, the `os` module is used.
    @param      csvFileName is the specified file name of the output .csv file
    @param      pngFileName is the specified file name of the output .png file    
    '''    
    # check if file exists 
    if os.path.exists(csvFileName):
        print('Removing previous .csv file')
        os.remove(csvFileName)
        
    if os.path.exists(pngFileName):
        print('Removing previous .png file')
        os.remove(pngFileName)

# recieve from uart, write to csv file
def writeCSV(fromNucleo,csvFileName):
    '''
    @brief      Method for writing a .csv file
    @details    As data is being transferred from the Nucleo to the PC, we want to append this data
                to a .csv file for later use/analyis. Initially, the string is already in a csv type format,
                but to write it to a csv file, we need to strip the string of line endings, split it on commas,
                convert to type `float`, then append a new row to the .csv file.
    @param      fromNucleo is the string read from uart for a given iteration
    @param      csvFileName is the specified file name of the output .csv file
    '''  
    csvStripped = fromNucleo.strip()                 # strip line endings
    csvSplit = csvStripped.split(',')               # split on commas
    
    timeValue = float(csvSplit[0])                  # float time and function values to be written to csv
    functionValue = float(csvSplit[1])
    
    with open(csvFileName,"a") as f:
        writer = csv.writer(f,delimiter=",")
        writer.writerow([timeValue,functionValue]) 
    
def createPlot(csvFileName,pngFileName):
    '''
    @brief      Method for creating a plot
    @details    This method utilizes the matplotlib module to plot data from a .csv file created
                earlier. The .csv file is used for plotting in conjunction with the numpy module,
                and then the plot is saved to a .png file in the working directory.
    @param      csvFileName is the specified file name of the output .csv file
    @param      pngFileName is the specified file name of the output .png file    
    '''  
    # formatting
    _myfont = {'fontname':'Times New Roman','fontsize':12}
    
    print('Plotting data')
    figure1 = plt.figure(figsize=(5, 3), dpi=200)                                                # create a figure
    x = []
    y = []
    x, y = np.loadtxt(csvFileName, delimiter=',', unpack=True)
    plt.plot(x,y,'k')
    plt.axis([0, max(x), 1.05*min(y), 1.05*max(y)])
    plt.xlabel('time, t [ s ]\n\nFigure 2. Exponentially decaying sinewave.',**_myfont)
    plt.ylabel('function value, y(t) [ - ]',**_myfont)
    plt.tight_layout()
    plt.show()
    figure1.savefig(pngFileName)
    print('Plotting completed')



if __name__ == "__main__":
    
    _csvName = "functionData.csv"
    _pngName = "decayFunction.png"
    
    clearFiles(_csvName,_pngName)
    
    ## @brief     Serial port definition
    #  @details   Need to specify the port to use for serial communication, as well as a baud rate
    ser = serial.Serial(port='COM5',baudrate=115273,timeout=1)
    ser.flushInput()
    
    # Tell the keyboard module to respond to these particular keys only
    ## @brief   Define callback keys
    #  @details Using `keyboard.on_release_key` with "s" and "g" to implement callbacks from key presses
    keyboard.on_release_key("s", callback=kb_cb)
    keyboard.on_release_key("g", callback=kb_cb)
    
    ## @brief   Initial value of last key released
    #  @details Setup such that key press callback is only checked if `lastKey` is not None.
    lastKey = None
    ##  @brief  Finite state machine variable for PC script
    pcState = 0
    ## @brief  Counter variable used to print certain information once to the user
    counter = 0
    
    userPrompt = True
    
    
    while True:
        
        try:
            

            
            if userPrompt == True:
                print('possible user commands')
            
            # start by checking for key press
            
            if lastKey is not None:    # if a key is pressed, transition to new state
                
                sendChar(lastKey)    
                print('You pressed ' + lastKey)
                if lastKey == 'g': 
                    pcState = 1
                elif lastKey == 's':
                    pcState = 2
                lastKey = None     # reset lastKey
                
                
            if pcState == 0:    # looking for g to be pressed
               
                if counter == 0:    # if it's the first time in pcState
                    print('Press "g" to begin collecting data')
                    counter += 1        # only want opening message once
                    
                    
            elif pcState == 1:      # looking for s or time elapsed to reach limit in main program
            ## @brief   String read from serial
            #  @details  Communication line from Nucleo to PC, used both to transmit actual data, as well as some commands
                fromNucleo = ser.readline().decode('ascii')      # read from uart
                
                if counter == 1:
                    print('Press "s" to stop collecting data')
                    counter += 1        
                
                if fromNucleo == 'ready\r\n':
                    pcState = 2
                
    
            elif pcState == 2:      # reading data from nucleo and writing to .csv file
                fromNucleo = ser.readline().decode('ascii')
                if counter == 2:
                    keyboard.unhook_all()       # no longer need user input
                    print('Writing to .csv file')
                    counter += 1
                    
                if fromNucleo:   # check that string is non-empty
                    if fromNucleo == 'plot':    # if nucleo sends signal that data has all been read  
                        pcState = 3
                    else:
                        writeCSV(fromNucleo,_csvName)    # write to csv file
                    
            
            elif pcState == 3:      # runs once to plot data, then breaks out of loop since all actions are completed
                createPlot(_csvName,_pngName)
                print('All actions completed, closing program')
                break
 
                

        except KeyboardInterrupt:
            print("Keyboard Interrupt")
            break
        
    
    ser.close()
    
