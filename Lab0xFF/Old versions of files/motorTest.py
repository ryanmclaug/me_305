'''
@file       motorTest.py
@brief      
@details    
            \n *See source code here:*  \n

@author     Ryan McLaughlin
@date       Originally created on 03/04/21 \n Last modified on 
'''

import pyb

# pin definitions
pinA15 = pyb.Pin ( pyb.Pin.cpu.A15 )    # nSleeps

pinB4 = pyb.Pin ( pyb.Pin.cpu.B4 )      # IN1

pinB5 = pyb.Pin ( pyb.Pin.cpu.B5 )      # IN2

pinB0 = pyb.Pin ( pyb.Pin.cpu.B0 )      # IN3

pinB1 = pyb.Pin ( pyb.Pin.cpu.B1 )      # IN4

# timer definition
tim3 = pyb.Timer(3, freq = 20000)

# channels
t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin = pinB4)

t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin = pinB5)

t3ch3 = tim3.channel(3, pyb.Timer.PWM, pin = pinB0)

t3ch4 = tim3.channel(4, pyb.Timer.PWM, pin = pinB1)


if __name__ == "__main__":
    
    pinA15.high()        
    pinB4.low() 
    
    while True:
        try:
        
            t3ch2.pulse_width_percent(20)
            
            
        except KeyboardInterrupt:       # press Ctrl-C to interupt 
            print('Ctrl-C has been pressed, exiting program')
            pinB5.low()          
            break
    