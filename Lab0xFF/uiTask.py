'''
@file       uiTask.py
@brief      Class for interaction with user interface on PC
@details    In this lab, we are learning how to use serial communication, which will allow us to run one script on the PC,
            and another on the Nucelo board. For the script that will run on the board, a class has been implemented to
            run the main finite state machine which will collect data and send that data back to the PC for post-processing.

            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/uiTask.py \n
            

@author     Ryan McLaughlin
@date       Originally created on 02/21/21 \n Last modified on 03/17/21
'''

import shares
import utime
from pyb import UART
import sys
import pyb
from array import array


## @brief   Assign uart port to a variable
myuart = UART(2) 
pyb.repl_uart(None)


class uiTask:
    '''
    @brief      Class for interaction with user interface on PC
    @details    In this lab, we are learning how to use serial communication, which will allow us to run one script on the PC,
            and another on the Nucelo board. For the script that will run on the board, a class has been implemented to
            run the main finite state machine which will collect data and send that data back to the PC for post-processing.
    '''
    
    # static variables here
    S0_INIT             = 0
    S1_READY_FOR_INPUT  = 1
    S2_COLLECT_DATA     = 2
    S3_PROCESS_DATA     = 3
    S4_NUCLEO_DONE      = 4
    
    
    def __init__(self, timeSpan , uiPeriod , encoderPeriod, debugFlag = False ):
        '''
        @brief  Constructor for UI task
        @param  timeSpan   Maximum allowable time before data collection is automatically terminated
        @param  uiPeriod   Operation period for UI
        @param  encoderPeriod   Controller/encoder/motor period, must be slower than UI period
        @param debugFlag    Debugging flag related to print statements in Putty
        '''
        
        
        
        ## @brief  Debugging flag for print statements in Putty
        self.debugFlag = debugFlag
        
        ## @brief       Element index for both collecting data and sending data
        self.currentElement = 1     # data point at t = 0 is preallocated before data collection begins
        
        ## @brief  Last element reached in data collection
        self.elementReached = 0  
        
        ## @brief       Variable to store current ascii character
        #  @details     Using UART to send data from the PC to the Nucelo, `val` holds the ascii
        #               number for appropriate keys.
        self.val = 0
        
        ## @brief       Finite state machine needs a state variable to regularly update as state transitions occur.
        self.state = 0   
        
        ## @brief       Time span to collect data
        #  @details     Once data collection begins, the two conditions to exit this process are:\n
        #               1) User presses the "s" key \n2) Program reaches a max time, specified by `timeSpan` (in microseconds)
        self.timeSpan = timeSpan            # after 30 seconds, data collection should stop
        
        ## @brief   Encoder/controller period
        #  @details Used in determining how large of an array to preallocate
        self.encoderPeriod = encoderPeriod
        
        ## @brief       Specify time between samples
        #  @details     Time between samples from given function, in microseconds. This sets the resolution of data collection
        self.uiPeriod = uiPeriod
        
        ## @brief  Previous time, used for period mechanism
        self.lastTime = utime.ticks_us()
        
        ## @brief  Next time, used for period mechanism
        self.nextTime = utime.ticks_add(self.lastTime, self.uiPeriod)
        
        ## @brief       Set refrence point for data collection (in microseconds)
        self.zeroTime = utime.ticks_us()        # refrence time for data collection
        
        ## @brief  Attribute for setting up array preallocation, based on encoder period and maximum time for data collection
        self.dataLength = int((self.timeSpan/self.encoderPeriod)+10)
        

        ## @brief       Array of time data points, of type `float`
        #  @details     `timeData` stores the time values from data collection in a pre allocated array. If data collection
        #               is stopped by the user before the max time is reached, empty data points are filtered out when sending
        #               csv data to the PC.
        self.timeData = array('f',[0]*self.dataLength)
        
        ## @brief       Array of encoder 1 position data points, of type `float`
        self.enc1PosData = array('f',[0]*self.dataLength)
        
        ## @brief       Array of encoder 2 position data points, of type `float`
        self.enc2PosData = array('f',[0]*self.dataLength)
        
        ## @brief       Array of encoder 1 speed data points, of type `float`
        self.enc1SpeedData = array('f',[0]*self.dataLength)
        
        ## @brief       Array of encoder 2 speed data points, of type `float`
        self.enc2SpeedData = array('f',[0]*self.dataLength)
        
        print('uiTask: initialized 5 arrays {:} columns'.format(self.dataLength))
        
        
        ## @brief  Boolean variable to register taking the first data point
        #  @details  Initially set to `True`, this variable only changes to false once the first data
        #            point is ready to be read from shares.py
        self.firstData = True
        
        ## @brief  Variable for storing the current message to be written to the PC
        #  @details  Filled with a string when a message is available, and set back to `None` after a message is sent
        self.currentMessage = None
        
        ## @brief  Current absolute time in microseconds
        self.currentTime = utime.ticks_us()
        
    
                
    # main run method  
    def run(self): 
        '''
        @brief      Main run method for UI task
        @details    Called as fast as possible from a main file, and operates on period defined in constructor
        '''
        
        self.currentTime = utime.ticks_us()
        
        if utime.ticks_diff(self.currentTime, self.nextTime) >= 0:
            print('uiTask: period reached')
            
            ## @details Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.uiPeriod)
            
            # this entire block only runs if period is reached
          
            print('uiTask: STATE: {:}, ELEMENT {:}'.format(self.state,self.currentElement))
                
            # start of finite state machine
            if self.state == self.S0_INIT:
                print('uiTask: Initializing')
                self.transitionTo(self.S1_READY_FOR_INPUT)      # always transition to state 1, then wait for user input
                 
                
            elif self.state == self.S1_READY_FOR_INPUT:
                
                # decode input from keyboard into variable val
                if myuart.any() != 0:
                    self.val = myuart.readchar()     # read from uart
                    print('uiTask: You sent an ASCII ' + str(self.val) + ' to the Nucleo')
                    print(self.val)
    
                    
                if self.val == 103:
                    print('uiTask: g has been pressed, data collection can now begin.')
                    shares.controller = True
                    print('uiTask: sending signal to turn on controller')
                    self.transitionTo(self.S2_COLLECT_DATA)   
                
                self.val = 0
            
            elif self.state == self.S2_COLLECT_DATA:
                
                if self.debugFlag == True:
                    print('uiTask: current element is' + str(self.currentElement) )
                
                # if shares.py has message to send to PC
                if len(shares.forPrint) != 0:
                    
                    self.currentMessage = str(shares.forPrint.pop(0))
                    if self.debugFlag == True:
                        print('uiTask: shares.forPrint not empty, printing to spyder console')
                        print('uiTask: self.currentMessage has the following message in first element\n'             # check that ouput is correct
                              + str(self.currentMessage))
                        print('uiTask: first letter of self.currentMessage is "' + str(self.currentMessage) + '"')
                    
                    myuart.write(self.currentMessage)  
                    self.currentMessage = None
                    
                    
                
                # this ensures that first data point corresponds to time = 0.0
                if self.firstData == True and len(shares.enc1Pos) != 0:
                    self.zeroTime = self.currentTime    # first data point taken at time = 0
                    self.firstData = False
                  
                ## @brief  Time since data collection began
                self.elapsedTime = utime.ticks_diff(self.currentTime, self.zeroTime)    # calculate elapsed time
                
                print('uiTask: elapsed time is ' + str(self.elapsedTime)) 
            
                if self.debugFlag == True:  # make sure that list never has more than 1 value
                    print('uiTask: length of encoder data list is ' + str(len(shares.enc1Pos)))
                
                # if time reaches time limit, move to processing data
                if self.elapsedTime > self.timeSpan:
                    print('uiTask: time limit has been reached, data collection has been terminated.')
                    self.elementReached = self.currentElement-1
                    self.currentElement = 0
                    shares.command.append("disableMotors")
                    shares.controller = False
                    self.transitionTo(self.S3_PROCESS_DATA)  
                
                    
                elif len(shares.enc1Pos) == 0:       # if list is empty
                    print('uiTask: no new data to read from encoder')
                    
                # if list is not empty, want to read data from shares.py and then delete that data point from list
                else:
                    print('uiTask: updating array of data to be sent')

                    self.timeData[self.currentElement] = self.elapsedTime/1000000
                    self.enc1PosData[self.currentElement] = shares.enc1Pos.pop(0)
                    self.enc2PosData[self.currentElement] = shares.enc1Omega.pop(0)
                    self.enc1SpeedData[self.currentElement] = shares.enc2Pos.pop(0)
                    self.enc2SpeedData[self.currentElement] = shares.enc2Omega.pop(0)
                    
                    self.currentElement += 1
                
                    
                if myuart.any() != 0:
                    self.val = myuart.readchar()     # read from uart
                    print('uiTask: You sent an ASCII ' + str(self.val) + ' to the Nucleo')
                    
                if self.val == 115:  
                    print('uiTask: s has been pressed, data collection has been terminated.')
                    self.elementReached = self.currentElement-1
                    self.currentElement = 0
                    shares.controller = False
                    shares.command.append("disableMotors")
                    self.transitionTo(self.S3_PROCESS_DATA)  
                    
                elif self.val == 122:
                    print('uiTask: z has been pressed, encoder 1 position reset to zero')
                    shares.command.append("zeroEnc1")
                
                elif self.val == 112:
                    print('uiTask: p has been pressed, printing current encoder 1 position in Spyder console')
                    shares.command.append("posEnc1")
                
                elif self.val == 100:
                    print('uiTask: d has been pressed, printing latest encoder 1 delta in Spyder console')
                    shares.command.append("deltaEnc1")
                
                elif self.val == 120:
                    print('uiTask: x has been pressed, encoder 2 position reset to zero')
                    shares.command.append("zeroEnc2")
                
                elif self.val == 111:
                    print('uiTask: o has been pressed, printing current encoder 2 position in Spyder console')
                    shares.command.append("posEnc2")
                
                elif self.val == 102:
                    print('uiTask: f has been pressed, printing latest encoder 2 delta in Spyder console')
                    shares.command.append("deltaEnc2")
                
                elif self.val == 118:
                    print('uiTask: v has been pressed, printing latest encoder 1 speed in Spyder console')
                    shares.command.append("speedEnc1")
                
                elif self.val == 119:
                    print('uiTask: w has been pressed, printing latest encoder 2 speed in Spyder console')
                    shares.command.append("speedEnc2")
                
                elif self.val == 114:
                    print('uiTask: r has been pressed, disabling both motors')
                    shares.command.append("disableMotors")
                
                
                self.val = 0
            
            
            elif self.state == self.S3_PROCESS_DATA:
                
                    
                    if self.currentElement <= self.elementReached:  # if data was actually filled

                        print('uiTask: sending encoder 1 data')
                        myuart.write('{:},{:},{:},{:},{:}\r\n'.format(self.timeData[self.currentElement],
                                                                      self.enc1PosData[self.currentElement],
                                                                      self.enc2PosData[self.currentElement],
                                                                      self.enc1SpeedData[self.currentElement],
                                                                      self.enc2SpeedData[self.currentElement]  ))                             
                        
                        self.currentElement += 1
                        
                    elif self.currentElement == self.elementReached + 1:
                        
                        myuart.write('J{:},{:},{:},{:}\r\n'.format(shares.Kp,
                                                                         shares.Ki,
                                                                         float(shares.J1sum/self.elementReached),
                                                                         float(shares.J2sum/self.elementReached)))
                        self.currentElement += 1
                        
                    else:
                        myuart.write('plot\r\n')        # send signal to PC to plot data
                        print('uiTask: done creating comma seperated print statments')
                        self.transitionTo(self.S4_NUCLEO_DONE)
                        
                    
    
    
            elif self.state == self.S4_NUCLEO_DONE:
                sys.exit()


    
        
        
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState is the state to transition to
        '''
        if self.debugFlag == True:
            print('uiTask: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            print('uiTask: State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state
        

        
        
        
        
   