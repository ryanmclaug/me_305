'''
@file       controllerTask.py
@brief      Controller task class
@details    This class uses instances of three different drivers for encoders, motors, and controllers.
            It is used in conjuction with a uiTask from the main file for this project
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/controllerTask.py \n


@author     Ryan McLaughlin
@date       Originally created on 03/02/21 \n Last modified on 03/17/21
'''

import shares
import utime
import pyb
from encoder import encoder
from motorDriver import motorDriver
from closedLoop import closedLoop
from array import array

class controllerTask:
    '''
    @brief      Controller task class
    @details    This class uses instances of three different drivers for encoders, motors, and controllers.
            It is used in conjuction with a uiTask from the main file for this project
    '''
    
    # static variables here
    ##  @brief Initial state
    S0_INIT             = 0
    ## @brief Controller off state
    S1_CONTROLLER_OFF   = 1
    ## @brief Controller operational state
    S2_CONTROLLER_ON    = 2

    
    
    def __init__(self, controllerPeriod, debugFlag = False):
        '''
        @brief  Constructor for controller task
        @param  controllerPeriod   Controller period, must be slower than UI period
        @param debugFlag    Debugging flag related to print statements in Putty
        '''
        
        ## @brief  First encoder object
        self.enc1 = encoder( pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4, debugFlag = True)
        
        ## @brief  Second encoder object
        self.enc2 = encoder( pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8, debugFlag = True) 

        ## @brief  First motor object
        self.mot1 = motorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2, 3, 1, debugFlag = True )
        
        ## @brief  Second motor object
        self.mot2 = motorDriver( pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4, 3, 2, debugFlag = True )
        
        ## @brief  Controller object
        self.control = closedLoop( self.KpPrime, self.KiPrime, debugFlag = True)
        
        ## @brief Velocity feedback constant
        self.KpPrime = 0.052
        
        ## @brief Position feedback constant
        self.KiPrime = 0.052
        
        ## @brief  Debugging flag for print statements in Putty
        self.debugFlag = debugFlag
        
        ## @brief       Finite state machine needs a state variable to regularly update as state transitions occur.
        self.state = 0   
        
        ## @brief   Controller period
        self.controllerPeriod = controllerPeriod
        
        ## @brief  Current absolute time in microseconds
        self.currentTime = utime.ticks_us()
        
        ## @brief       Set refrence point for data collection (in microseconds)
        self.zeroTime = utime.ticks_us()
        
        ## @brief  Previous time, used for period mechanism
        self.lastTime = utime.ticks_us()
        
        ## @brief  Next time, used for period mechanism
        self.nextTime = utime.ticks_add(self.lastTime, self.controllerPeriod)
        
        ## @brief  Index variable for interpolation
        #  @details  Instead of searching through the entire reference data set, start at the last index value
        self.lastIndex = 0
        
        ## @brief  J summation portion for encoder 1
        #  @details This value is iterated upon each time the controller is updated
        self.J1 = 0
        
        ## @brief  J summation portion for encoder 2
        self.J2 = 0
        
        ## @brief   Array for storing refrence data time values, holds type float
        self.tRef = array('f',[0]*601)
        
        ## @brief   Array for storing refrence data position values, holds type float
        self.posRef = array('f',[0]*601)
        
        ## @brief   Array for storing refrence data speed values, holds type float
        self.speedRef = array('f',[0]*601)
        
    def run(self):
        '''
        @brief      Main run method for controller task
        @details    Called as fast as possible from a main file, and operates on period defined in constructor
        '''
        
        self.currentTime = utime.ticks_us()
        
        if utime.ticks_diff(self.currentTime, self.nextTime) >= 0:
            print('controllerTask: controllerPeriod reached')
            self.nextTime = utime.ticks_add(self.nextTime, self.controllerPeriod)
  
            print('controllerTask: STATE: {:}'.format(self.state))
            
            
            # i) update encoders
            
            self.enc1.update()      # always update encoders? (on controller period)
            self.enc2.update()  
            
            if self.state == self.S0_INIT:
                print('controllerTask: Initializing')
                shares.Kp = self.KpPrime
                shares.Ki = self.KiPrime
                self.mot1.enable()
                self.loadData('modifiedRef.csv')
                self.transitionTo(self.S1_CONTROLLER_OFF)

            
            elif self.state == self.S1_CONTROLLER_OFF:
                if shares.controller == True:
                    self.transitionTo(self.S2_CONTROLLER_ON)
                    self.zeroTime = self.currentTime
            
            
            elif self.state == self.S2_CONTROLLER_ON:
                
                ## @brief  Time since data collection began
                self.elapsedTime = float(utime.ticks_diff(self.currentTime, self.zeroTime)/1000000)
                
                # ii) desired values
                
                
                if self.elapsedTime < 15:
                    self.encPosRef, self.encOmegaRef, self.lastIndex= self.interpolate(self.elapsedTime, self.lastIndex)
                
                
                if self.debugFlag == True:
                    print('controllerTask: current thetaRef = ' + str(self.encPosRef))
                    print('controllerTask: current omegaRef = ' + str(self.encOmegaRef))

                
                
                # iii) get position
                
                ## @brief  Current encoder 1 position
                self.enc1Pos = self.enc1.get_position()
                
                ## @brief  Current encoder 2 position
                self.enc2Pos = self.enc2.get_position()                    
                
                if self.debugFlag == True:
                    print('controllerTask: current theta1 = ' + str(self.enc1Pos))
                    print('controllerTask: current theta2 = ' + str(self.enc2Pos))
                
                # iv) get speed
                
                ## @brief  Current encoder 1 speed
                self.enc1Omega = self.enc1.get_speed()
                
                ## @brief  Current encoder 2 speed
                self.enc2Omega = self.enc2.get_speed()
                
                
                if self.debugFlag == True:
                    print('controllerTask: current omega1 = ' + str(self.enc1Omega))
                    print('controllerTask: current omega2 = ' + str(self.enc2Omega))
                
                self.J1 += (self.encOmegaRef - self.enc1Omega)**2 + (self.encPosRef - self.enc1Pos)**2
                self.J2 += (self.encOmegaRef - self.enc2Omega)**2 + (self.encPosRef - self.enc2Pos)**2
                
                
                # vi) update controller
                
                ##  @brief   New motor 1 duty cycle, to be sent to motor object
                self.mot1DutyNew = self.control.update(self.encOmegaRef, self.enc1Omega, self.encPosRef, self.enc1Pos)
                
                ##  @brief   New motor 2 duty cycle, to be sent to motor object
                self.mot2DutyNew = self.control.update(self.encOmegaRef, self.enc2Omega, self.encPosRef, self.enc2Pos)
                
                if self.debugFlag == True:
                    print('controllerTask: new self.mot1Duty = ' + str(self.mot1DutyNew))
                    print('controllerTask: new self.mot2Duty = ' + str(self.mot2DutyNew))
                
                # vii) set duty
                    
                    
                    self.mot1.setDuty(self.mot1DutyNew)
                    self.mot2.setDuty(self.mot2DutyNew)
                
                #  sharing data
                
                shares.enc1Pos.append(self.enc1Pos)    # place value in list, to be read by uiTask
                shares.enc2Pos.append(self.enc2Pos)    # place value in list, to be read by uiTask
                shares.enc1Omega.append(self.enc1Omega)    # place value in list, to be read by uiTask
                shares.enc2Omega.append(self.enc2Omega)    # place value in list, to be read by uiTask
                shares.J1sum = self.J1
                shares.J2sum = self.J2
  
            # if shares.command is something besides None, want to obey that command
            if len(shares.command) != 0:
                
                if self.debugFlag == True:
                    print('controllerTask: keyboard command sent from shares')
                currentCommand = shares.command.pop(0)
                
                # commands for encoder 1
                if currentCommand == "zeroEnc1":
                    if self.debugFlag == True:
                        print('controllerTask: zero encoder 1')
                    self.enc1.set_position(0)
                    shares.forPrint.append("Encoder 1 postiion reset to zero\r\n")
                    
                elif currentCommand == "posEnc1":
                    if self.debugFlag == True:
                        print('controllerTask: getting position for encoder 1')
                    shares.forPrint.append("Encoder 1 position is " + str(self.enc1.get_position()) + " deg\r\n")
                
                elif currentCommand == "deltaEnc1":
                    if self.debugFlag == True:
                        print('controllerTask: getting delta for encoder 1')
                    shares.forPrint.append("Encoder 1 delta is " + str(self.enc1.get_delta()) + " deg\r\n")
                
                elif currentCommand == "speedEnc1":
                    if self.debugFlag == True:
                        print('controllerTask: getting speed for encoder 1')
                    shares.forPrint.append("Encoder 1 speed is " + str(self.enc1.get_speed()) + " rpm\r\n")
                
                
                # commands for encoder 2
                elif currentCommand == "zeroEnc2":
                    if self.debugFlag == True:
                        print('controllerTask: zero encoder 2')
                    self.enc2.set_position(0)
                    shares.forPrint.append("Encoder 2 postiion reset to zero")
                    
                elif currentCommand == "posEnc2":
                    if self.debugFlag == True:
                        print('controllerTask: getting position for encoder 2')
                    shares.forPrint.append("Encoder 2 position is " + str(self.enc2.get_position()) + " deg\r\n")
                
                elif currentCommand == "deltasEnc2":
                    if self.debugFlag == True:
                        print('controllerTask: getting delta for encoder 2')
                    shares.forPrint.append("Encoder 2 delta is " + str(self.enc2.get_delta()) + " deg\r\n")
                
                elif currentCommand == "speedEnc2":
                    if self.debugFlag == True:
                        print('controllerTask: getting speed for encoder 2')
                    shares.forPrint.append("Encoder 2 speed is " + str(self.enc2.get_speed()) + " rpm\r\n")
                
                elif currentCommand == "disableMotors":
                    self.mot1.disable()
                    self.mot2.disable()
                    if self.debugFlag == True:
                        print('controllerTask: disabling motors')
                    shares.forPrint.append("E: motors have been disabled")
                
                if self.debugFlag == True:
                    print('controllerTask: added the following string to shares.forPrint -> ' + str(shares.forPrint[-1]) )
                
                
            if shares.controller == False:
                    self.transitionTo(self.S1_CONTROLLER_OFF)
                
    
    
    def transitionTo(self, newState):      # state transition method
        '''
        @brief      Method for transitioning states
        @details    The method will reassign the `self.state` variable when
                    directed to transition.
        @param      newState is the state to transition to
        '''
        if self.debugFlag == True:
            print('controllerTask: Transitioning from ' + 'S' + str(self.state) + '->' + 'S' + str(newState) )
            print('controllerTask: State transition occured at t = ' + str(self.currentTime) )
        self.state = newState               # now that transition has been declared, update state
        
        
    def loadData(self, refFileName):
        '''
        @brief      Method for loaading resampled data into arrays
        @details    This method strips and splits the reference data from a csv file,
                    and places time, position, and speed data into seperate arrays for use in interpolation
        @param      refFileName   File name for CSV loaded onto Nucleo that contains reference data (resampled)
        '''
        with open(refFileName) as file:
            n = 0
            for line in file:
                
                line = line.strip('\n')
                line = line.strip('\r')
                line = line.split(',')
                
                print('controllerTask: line is ' + str(line))
                print ('controllerTask: line n = ' + str(n))
                
                tRef0 = float(line[0])                  # float time and function values to be written to csv
                posRef0 = float(line[1])
                speedRef0 = float(line[2])
                
                self.tRef[n] = tRef0
                self.posRef[n] = posRef0
                self.speedRef[n] =speedRef0
                n += 1
    
    
    
    def interpolate(self, t0, iStart):
        '''
        @brief      Method for interpolating from reference data
        @details    This method quickly interpolates using a short for loop (should only run through a
                    few possibilites before reaching the correct value) from the reference data set.
        @param      t0  Time query point for interpolation
        @param      iStart   Last index value from interpolation, used as a starting point for next interpolation
                    speed up code
        @return     Returns the interpolated values for position and speed, as well as the last index value
                    to start at in the next cycle
        '''       
        
        # t0 in seconds, not microseconds
        print('controllerTask: time value to interpolate at is '+ str(t0))
        i = iStart
        diff = 1
        while diff > .025:      # if difference is greater than data resolution
            print('controllerTask: i = ' + str(i))
            tRef = float(self.tRef[i])
            print('controllerTask: time value being compared against is ' + str(tRef))
            diff = t0 - tRef
            print('controllerTask: diff = {:}'.format(diff))
            i += 1
        
        
        i1 = i-1
        i2 = i-0
        print('controllerTask: interpolating between index {:} and {:} of reference data'.format(i1,i2))
        t1 = float(self.tRef[i1])
        t2 = float(self.tRef[i2])
        
        pos1 = float(self.posRef[i1])
        pos2 = float(self.posRef[i2])
        
        speed1 = float(self.speedRef[i1])
        speed2 = float(self.speedRef[i2])
        
        posRef = pos1 + ((pos2 - pos1) / (t2 - t1)) * (tRef - t1) 
        speedRef = speed1 + ((speed2 - speed1) / (t2 - t1)) * (tRef - t1) 
        print('controllerTask: interpolated values are: posRef = {:} and speedRef = {:} of reference data'.format(posRef, speedRef))
        
        return posRef, speedRef, i1
        
        
        
        