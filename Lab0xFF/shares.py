'''
@file       shares.py
@brief      Used solely for sharing variables between files
@details    
            \n *See source code here:* https://bitbucket.org/ryanmclaug/me_305/src/master/Lab0xFF/shares.py  \n

@author     Ryan McLaughlin
@date       Originally created on 02/27/21 \n Last modified on 03/17/21
'''

## @brief       queue for commands sent from the UI task   
command = []

## @brief     queue for strings to be printed in the Spyder console,
#             based on commands from the UI
forPrint = []

## @brief       queue for encoder 1 position
enc1Pos = []

## @brief       queue for encoder 2 position    
enc2Pos = []

## @brief       queue for encoder 1 speed/omega
enc1Omega = []

## @brief       queue for encoder 2 speed/omega           
enc2Omega = []

## @brief  Kp value used by controller task          
Kp = 0

## @brief  Ki value used by controller task                  
Ki = 0

## @brief    summation portion of J for encoder/motor 1    
J1sum = 0

## @brief    summation portion of J for encoder/motor 2          
J2sum = 0

## @brief        Boolean variable to be set by the UI task and read by the controller task
controller = False