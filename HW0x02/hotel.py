'''
@file       hotel.py
@brief      hotel elevator system simulation script
@details    main script for testing usage of elevator class
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on DD/MM/YY \n Last modified on DD/MM/YY
'''



import utime
from elevatorTask import elevatorTask   



if __name__ == "__main__":
    
    # create elevator tasks
    elevator1 = elevatorTask(1, 500000, debugFlag = True)       # period of 0.5 s
    elevator2 = elevatorTask(2, 1000000, debugFlag = True)      # period of 1.0 s

    
    while True:
        try:
            
            elevator1.run()
            elevator2.run()
            
        
        except KeyboardInterrupt:
            # this except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-C has been pressed, exiting program')
            break