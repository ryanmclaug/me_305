'''
@file       elevatorTask.py
@brief      Class for elevator of HW0x02
@details    Recreating finite state machine model of elevator, but this time implementing
            classes and better code.
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on 02/03/21 \n Last modified on 02/03/21
'''

import random
import utime


class elevatorTask:
    
    # static variables defined before constructor
    
    S0_INIT = 0
    S1_MOVING_DOWN = 1
    S2_STOPPED_FLOOR_1 = 2
    S3_MOVING_UP = 3
    S4_STOPPED_FLOOR_2 = 4
    
    
    def __init__(self, elevatorNum, period, debugFlag = False):
        '''@brief   constructor
        '''
        
        self.state = 0      # initialize state
        self.runs = 0       # initialize how many runs have occured
        
        self.elevatorNum = elevatorNum      # allows setup of multi-elevator system
        
        self.debugFlag = debugFlag
        
        
        # setup timing system
        self.lastTime = utime.ticks_us()        # time stamp of last iteration in μs
        
        self.period = period        # run based on defined frequency
        
        self.nextTime = utime.ticks_add(self.lastTime, self.period)     # time stamp for next iteration to begin
        
        
        
        
        
    # define class methods
    
    def run(self):
        # timing setup
        
        thisTime = utime.ticks_us()
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:       # if time exceeds nextTime, the time stamp for when a new
                                                                # iteration should begin, then need to calculate new nextTime
            self.nextTime = utime.ticks_add(self.nextTime, self.period)       # update what next time should be                                             
              
            self.runs += 1        # when this loop is entered, it means a new run is commencing
              
            if self.debugFlag == True:        # if debugging is on, want to print more stuff
                  print('T' + str(self.elevatorNum) + ': S' + str(self.state) + ': R' + str(self.runs))
              
              
            # now actual finite state machine code is implemented
        
            if self.state == self.S0_INIT:
                # run state 0 (init)code
                
                self.button_1() == 0     # reset buttons
                self.button_2() == 0
                
                self.motor('DOWN')            # move down to first floor
                self.transitionTo(self.S1_MOVING_DOWN)
            
            
            elif self.state == self.S1_MOVING_DOWN:
                #run state 1 (moving down)
                
                if self.first():     # if first floor proximity sensor is tripped by being true, stop
                    self.motor('STOP')
                    self.button_1()==0   # at first floor, so reset button_1
                    self.transitionTo(self.S2_STOPPED_FLOOR_1)
                
                
            elif self.state == self.S2_STOPPED_FLOOR_1:
                 #run state 2 (stopped on floor 1)
                 
                 # if button for 2nd floor is pressed, move up
                 if self.button_2():
                     print('User has requested floor 2 for elevator ' + str(self.elevatorNum) )
                    
                     self.motor('UP')        
                     self.transitionTo(self.S3_MOVING_UP)
                 
                    
            elif self.state == self.S3_MOVING_UP:
                #run state 3 (moving up)
                
                # if we are at floor 2, stop the elevator
                if self.second():
                    self.motor('STOP')     # stop
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                
                
            elif self.state == self.S4_STOPPED_FLOOR_2:
                #run state 4 (stopped on floor 2)
                
                # if button for 1st floor is pressed, move down
                if self.button_1():
                    print('User has requested floor 1 for elevator ' + str(self.elevatorNum) )
                    
                    self.motor('DOWN')         # down
                    self.transitionTo(self.S1_MOVING_DOWN)
    
    
    # transition method
    
    def transitionTo(self, newState):
        if self.debugFlag == True:
            print( str(self.state) + '->' + str(newState) )
        self.state = newState               # now that transition has been declared, update state
    
    
    
    # elevator motor
    def motor(self, cmd):

        if cmd=='STOP':
            print('Elev stopped')
            
        elif cmd=='UP':
            print('Elev up')
            
        elif cmd=='DOWN':
            print('Elev down')
            
      
    # elevator buttons      
    def button_1(self):
       return random.choice([True,False])
    
    def button_2(self):
        return random.choice([True,False]) 
    
    
    # proximity sensors
    def first(self):
        return random.choice([True,False]) 
    
    def second(self):
        return random.choice([True,False]) 
                