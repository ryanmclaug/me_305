import random
import utime

class windshieldTask:
    
    # Static variables are defined outside of methods
    
    ## State 1 constant
    S0_INIT          = 0
    S1_MOVING_LEFT   = 1
    S2_STOPPED_LEFT  = 2
    S3_MOVING_RIGHT  = 3
    S4_STOPPED_RIGHT = 4
    
    def __init__(self, taskNum, period, DBG_flag=True):
        # This not your state 0
        
        ## The current state of the finite state machine
        self.state = 0
        
        ## Counts the number of runs of our task
        self.runs = 0
        
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Timestamp for last iteration of the task in microseconds
        #  could eliminate for concision if desired
        self.lastTime = utime.ticks_us()
        
        ## The period for the task in microseconds
        self.period = period
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ## An integer identifying the task
        self.taskNum = taskNum
    
    def run(self):
        thisTime = utime.ticks_us()
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            
            self.runs += 1
            if self.DBG_flag:
                print('T' + str(self.taskNum) + ': S' + str(self.state) + ": R" + str(self.runs))
            
            # main program code goes here
            if self.state==self.S0_INIT:
                # run state 0 (init) code
                self.motor_cmd('FWD') # Command motor to go forward
                self.transitionTo(self.S1_MOVING_LEFT)   # Updating state for next iteration
                
            elif self.state==self.S1_MOVING_LEFT:
                # run state 1 (moving left) code
                # If we are at the left, stop the motor and transition to S2
                if self.L_sensor():
                    self.motor_cmd('STOP')
                    self.transitionTo(self.S2_STOPPED_LEFT)
                
            elif self.state==self.S2_STOPPED_LEFT:
                # run state 2 (stopped at left) code
                # if go button is pressed, start motor and transition to S3
                if self.go_button():
                    self.motor_cmd('REV')
                    self.transitionTo(self.S3_MOVING_RIGHT)
                
            elif self.state==self.S3_MOVING_RIGHT:
                # run state 3 (moving right) code
                # If we are at the right, stop the motor and transition to S4
                if self.R_sensor():
                    self.motor_cmd('STOP')
                    self.transitionTo(self.S4_STOPPED_RIGHT)
                
            elif self.state==self.S4_STOPPED_RIGHT:
                # run state 4 (stopped at right) code
                self.motor_cmd('FWD') # Command motor to go forward
                self.transitionTo(self.S1_MOVING_LEFT)   # Updating state for next iteration
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
    
    def transitionTo(self, newState):
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
    
    def L_sensor(self):
        return random.choice([True, False]) # randomly returns T or F
    
    def R_sensor(self):
        return random.choice([True, False]) # randomly returns T or F
    
    def go_button(self):
        return random.choice([True, False]) # randomly returns T or F
    
    def motor_cmd(self, cmd):
        '''@brief Commands the motor to move or stop
           @param cmd The command to give the motor
        '''
        if cmd=='FWD':
            print('Mot forward')
        elif cmd=='REV':
            print('Mot reverse')
        elif cmd=='STOP':
            print('Mot stop')