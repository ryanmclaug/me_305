''' @file           food.py
    @brief          Implements a food class in Python.
    @details        This file is an example for ME305 to show implementation
                    of a simple class representing a food item to be eaten.
    @author         Charlie Refvem
    @date           01/27/20
    @copyright      Do with this example as you wish.
'''

class food:
    ''' @brief          A food object is something that can be eaten.
        @details        An object of class food.food has attributes and methods
                        reflecting the properties and behavior of real food.
                        The objects can be used with the other class in this
                        example, dog.dog, to show how classes can be used
                        together.
    '''
    
    def __init__(self, name, calories):
        ''' @brief          Constructs a food.food object.
            @details        The food.food object can then be eaten by an object
                            of another class such as food.food.
            @param name     The name of the food item.
            @param calories The calorie content of the food item in kcal.
        '''
        
        ## The name of the food item
        self.name = name
        
        ## The name of the food item
        self.calories = calories