''' @file           dog.py
    @brief          Implements a dog class in Python.
    @details        This file is an example for ME305 to show implementation
                    of a simple class representing a dog.
    @author         Charlie Refvem
    @date           01/27/20
    @copyright      Do with this example as you wish.
'''

class dog:
    ''' @brief          A dog is a member of the species Canis familiaris.
        @details        An object of class dog.dog has attributes and methods
                        reflecting the properties and behavior of real dogs.
                        the objects can be used to show that multiple instances
                        of dog.dog can be created using the same Python class.
    '''
    
    def __init__(self, name, weight, breed, age):
        ''' @brief          Constructs a dog.dog object.
            @details        This constructor allows the dogs name, size, breed,
                            and age to be specified when the dog.dog object is
                            created.
            @param name     The name of the dog as a string object.
            @param weight   A float object representing the weight of the dog
                            in pounds.
            @param breed    The breed of the dog as a string object.
            @param age      The age of the dog as an int object. The age is
                            represented using a Unix Timestamp representing the
                            number of seconds since Jan 01, 1970.
        '''
        
        ##  The name of the dog as a string object.
        self.name = name
        
        ##  The weight of the dog as an int object representing the weight of
        #   the dog in pounds.
        self.weight = weight
        
        ##  The breed of the dog as a string object.
        self.breed = breed
        
        ##  The age of the dog as an int object in Unix Timestamp format.
        self.age = age
        
        ##  A boolean indicating if the dog is has eaten recently.
        self.hasEaten = False
    
    def feed(self, food):
        ''' @brief          Feeds the dog
            @param food     The food to feed the dog. Must be a food.food
                            object.
        '''
        print(food.name + ' is tasty!')
        self.hasEaten = True
        self.weight += food.calories / 3000
    
    def getBark(self):
        ''' @brief          Query the dog's bark.
            @return         A string representing the dog's bark
        '''
        if self.breed == "Husky":
            bark = 'Ah-Wooooooo!'
        elif self.breed == "Terrier":
            bark = 'Yip Yip!'
        else:
            bark = 'Woof!'
        
        if self.weight < 20:
            return bark.casefold()
        else:
            return bark