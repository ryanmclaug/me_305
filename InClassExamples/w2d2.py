'''
@file       filename.py
@brief      one line description here
@details    detailed description here
            
            \n See source code here: 
            \n See <otherlinks> here:
@author     Ryan McLaughlin
@date       DD/MM/YY
'''

import dog

myDog = dog.dog('Chloe',65,'Australian Cattle Dog',8)

yourDog = dog.dog('Killer',6,'Maltese',9)

# someFood = food.food("Kibbles 'n' bits", 500)

# print(myDog.name + "weighs" + str(myDog.weight) )
# myDog.feed(someFood)
# print(myDog.name + "weighs" + str(myDog.weight) )

anotherDog = dog.dog('Fido',6,'Maltese')