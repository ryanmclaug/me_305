''' @file           main.py
    @brief          Shows example useage of dog.dog and food.food.
    @details        This file is an example for ME305 to show usage of a couple
                    simple Python classes.
    @author         Charlie Refvem
    @date           01/27/20
    @copyright      Do with this example as you wish.
'''

import dog, food

myDog = dog.dog('Chloe', 65, 'Australian Cattle Dog', 7)
yourDog = dog.dog('Henry', 15, 'Terrier', 2)
myFood = food.food('Pumpkin', 300)

print(myDog.name + " weighs " + str(myDog.weight))

myDog.feed(myFood)

print(myDog.name + " weighs " + str(myDog.weight))

print(myDog.name + " says " + myDog.getBark())
print(yourDog.name + " says " + yourDog.getBark())