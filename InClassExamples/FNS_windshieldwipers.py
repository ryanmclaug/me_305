'''@file    FNS_windshieldwipers.py
@brief      Simulates set of windshield wipers
@details    Implements a finite state machine, shown below, to simulate
            the behavior of a set of wipers
@author     Ryan Mclaughlin
@date       01/19/21
@copyright  License info here
'''



import time
import random

# function definitions here
# this way things can be reused

def motor_cmd(cmd):     # just replace this dummy code
                        
    if cmd=='FWD':
        print('Mot forward')
    elif cmd=='REV':
        print('Mot reverse')
    elif cmd=='STOP':
        print('Mot stop')

def L_sensor():
    return random.choice([True,False]) # randomly returns true or false

def R_sensor():
    return random.choice([True,False]) # randomly returns true or false

def go_button():
    return random.choice([True,False]) # randomly returns true or false


# main program/test program begin
# this code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    #program initialization goes here
    state = 0   # intial state is the init state
    
    while True:
        try:
            # main program code goes here
            
            if state==0:
                # run state 0 (init)code
                print('S0')
                motor_cmd('FWD')
                state = 1   # updating state for next iteration
                
            elif state==1:
                #run state 1 (moving left)
                print('S1')
                # if we are at the left, stop the motor and transition to S2
                if L_sensor():
                    motor_cmd('STOP')
                    state = 2
                
            elif state==2:
                 #run state 2 (stopped left)
                 print('S2')
                 # if go button is pressed, start motor and transition to S3
                 if go_button():
                     motor_cmd('REV')
                     state = 3
                 
            elif state==3:
                #run state 3 (moving right)
                print('S3')
                # if we are at the right, stop the motor and transition to S4
                if R_sensor():
                    motor_cmd('STOP')
                    state = 4
                
            elif state==4:
                #run state 4 (stopped right)
                print('S4')
                motor_cmd('FWD')    # command motor to go forward
                state = 1
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            # slow down execution of FSM so we can see output in console
            time.sleep(0.3)
            
        except KeyboardInterrupt:
            # this except block catches "Ctrl-C" from the keyboard to end the 
            # while(True) loop when desired
            print('Ctrl-C has been pressed')
            break
        
    #program de-initialization goes here
