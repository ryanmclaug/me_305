'''
@file       filename.py
@brief      one line description here
@details    detailed description here
            
            \n See source code here: 
            \n See <otherlinks> here:
@author     Ryan McLaughlin
@date       DD/MM/YY
'''

class dog:
    '''@brief 
    '''
    # define constructor, with specfic name
    def __init__(self, dogName, dogWeight, dogBreed = 'Retriever', dogAge=0):        # always self as first parameter
        '''@brief docstring of constructor
        '''
        
        ## The name of the dog
        self.name = dogName     # assigning input argument (dogName) to
                                # class attribute (self.name)
                                
        self.weight = dogName     
                            
        ## The name of the dog
        self._breed = dogBreed     # starting a variable name with an underscore
                                   # makes it "private", or only accesible from
                                   # within the same class. Private variables
                                   # should not appear in doxygen either.         
        
        self.age = dogAge
                                
    def feed(self, food2eat):
        print("That was a tasty" + food2eat.name)
        self.weight += food2eat.calories/3000
        
    def setBreed(self, newBreed):
        self._breed = newBreed
        