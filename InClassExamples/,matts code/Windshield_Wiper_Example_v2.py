'''@file        Windshield_Wiper_Example.py
   @brief       Simulates a set of windshield wipers
   @details     Implements a finite state machine, shown below, to simulate the
                behavior of a set of wipers.
   @author      Matthew Frost
   @date        1/19/2021
   @copyright   License Info Here
'''

import time
from windshieldTask import windshieldTask

# Main program / test program begin:
# This code only runs if the script is executed as main by pressing play
#but doesn not run if the script is imported as a module
if __name__ == "__main__":
    # Program initialization goes here
    task1 = windshieldTask(1, 200000, DBG_flag=False)
    
    task2 = windshieldTask(2, 1000000, DBG_flag=True)
    
    while True: 
        try: 
            # print('T1:')
            task1.run()
            # print('T2:')
            # task2.run()
            # Slow down execution of FSM so we can see output in console   
            # time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization goes here
    print('After')
    
    
    
    
    
    
    
    
    
    
    