# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 13:55:33 2021

@author: rmcla
"""


byte_4=0b10000000
byte_5=0b11111111
print(byte_5)

bool_var = True
# bool_var = byte_4&(1<<7)
print(bool_var)
      
# if byte_4&(1<<7) = True:
#     byte_5&=~(0b00001111)  
# else: 
#     byte_5&=~(0b11110000)

# print(byte_5)