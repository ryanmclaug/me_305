'''
@file       Lab0x03_main.py
@brief      File to run instance of class `game.py`
@details    The Nucleo Says game is designed using a class structure, therefore we need
            a supplemental main script to create an instance of the game and call the
            `run` method. This file also includes some initial game prompts/instructions.
            
            \n *See source code here:* https://bitbucket.org/ryanmclaug/matt_ryan_shared/src/master/Lab0x03_main.py \n
@author     Ryan McLaughlin
@author     Matthew Frost
@date       Originally created on 02/04/21 \n Last modified on 02/16/21
'''

from game import game


if __name__ == "__main__":
    # create an instance of the class 'game.' 
    
    # class syntax ( unit, tol, setLength, maxTime, debugFlag )
    
    task1 = game(500000, 400000, 2, 5000000, False)     
    
    print('Welcome to Nucleo Says: The ME 305 Original Game. To play this',
          'game, the instructions are as follows: \n \n')
            # more instructions
    print('- After a short countdown, carefully watch as the LED flashes a pattern. \n',
            '- Once the pattern is complete, it is your turn to replicate the pattern using the blue button. \n',
            '- If you succesfully match the LED pattern, you have succesfully completed a set. \n',
            '- If you make it through all ', str(task1.setLength),
            ' sets of matching, you have won a round! \n',
            '- Once a round is over, you will have the choice to play again or exit the game. \n \n',
            'Have fun, and let the games begin...')
    
    while True: 
        try: 

            task1.run() 

            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            task1.reset(True,True,True,True,True,True,True,True)
            break
    
    # Program de-initialization
    print('Thanks for playing Nucleo Says: The ME 305 Original Game. See you next time!')
