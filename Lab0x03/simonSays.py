'''
@file       simonSays.py
@brief      main script for simon says game
@details    detailed description here
            
            \n *See source code here:* <insert link> \n
@image      html filename.jpg “Caption” width = xx% 
@author     Ryan McLaughlin
@date       Originally created on 02/04/21 \n Last modified on 02/04/21
'''

from game import game

micropython.alloc_emergency_exception_buf(100)  # display more error messages


if __name__ == "__main__":
    
    # begin with welcome message, with instructions on how to play the game
    print('Welcome to Simon Says: The Nucleo Original Game. To play this',
          'game, the instructions are as follows: \n \n')
    # more print statements with instructions
    print('- When you are ready to play, press the blue user button on the Nucleo board.\n',
          '- After a short countdown, an LED pattern will be displayed on the board. \n',
          '- Once the pattern is complete, it is your turn to press the button in the same sequence. \n',
          '- If you succesfully match the LED pattern, an appended pattern will be displayed. \n',
          '- If you make it through n sets of matching, you have won a round! \n',
          '- Once a round is over, you will have the choice to play again or exit the game. \n \n',
          'Have fun, and let the games begin...')

    
    # definition of tasks from my class(es) go here
    
    
    
    
    while True: 
        try: 
            pass
            # run task(s) here
            game1 = game( 3, 1000000, debugFlag = True )
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard
            # to end the while(True) loop while desired
            break
    
    # Program de-initialization goes here
    # basic exit message
    print('Thanks for playing Simon Says: The Nucleo Original Game. \n')
    # add final results (W/L) print statement based on counter variables here
    print('Your final record was')